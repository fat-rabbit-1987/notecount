That is Maven plugins.

You can count your project note if you use it.

First, you want to configuration you project in pom.xml file.

	<build>
		<finalName>stu</finalName>
		<plugins>
			<plugin>
				<groupId>cn.haohaowo</groupId>
				<artifactId>maven-notecount-plugin</artifactId>
				<configuration>
					<includes>
						<include>java</include>
					</includes>
					<basedir>${project.basedir}</basedir>
					<sourceDirectory>${project.build.sourceDirectory}</sourceDirectory>
					<testSourceDirectory>${project.build.testSourceDirectory}</testSourceDirectory>
					<resources>${project.build.resources}</resources>
					<testResources>${project.build.testResources}</testResources>
				</configuration>
			</plugin>
		</plugins>
	</build>

Parameter
	include, that is type file you want to count. Default type, java, xml, properties.
	basedir, your project base directory.
	sourceDirectory, your project code directory.
	testSourceDirectory, your project test code directory.
	resources, your project resource directory. Example, xml, properties.
	testResources, your project test resource directory. Example, xml, properties.

Run mvn command.
mvn cn.haohaowo:maven-notecount-plugin:0.0.1:notecount -e



If you want to run short mvn commad. You would configuration setting.xml, not gobal, only yourself. 

	<pluginGroups>
		<!-- pluginGroup | Specifies a further group identifier to use for plugin 
			lookup. <pluginGroup>com.your.plugins</pluginGroup> -->
			<pluginGroup>cn.haohaowo</pluginGroup>
	</pluginGroups>
But, I does not upload that plugin on maven repo.
